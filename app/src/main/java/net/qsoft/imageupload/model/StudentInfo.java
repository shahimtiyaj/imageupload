package net.qsoft.imageupload.model;

public class StudentInfo {

    private String SName;
    private String SImage;

    public StudentInfo(String SImage) {
        //this.SName = SName;
        this.SImage = SImage;
    }

    public String getSName() {
        return SName;
    }

    public void setSName(String SName) {
        this.SName = SName;
    }

    public String getSImage() {
        return SImage;
    }

    public void setSImage(String SImage) {
        this.SImage = SImage;
    }

}
