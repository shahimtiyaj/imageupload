package net.qsoft.imageupload;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import net.qsoft.imageupload.Database.DAO;
import net.qsoft.imageupload.app.AppController;
import net.qsoft.imageupload.model.StudentInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import static net.qsoft.imageupload.Database.DBHelper.TABLE_STUDENT_INFO;

public class MainActivity extends AppCompatActivity {

    private static final int RESULT_LOAD_IMG_FROM_GALLERY = 2;
    private static final int CAMERA_REQUEST = 1;
    private static final int PICK_FROM_GALLERY = 2;
    private static final String TAG = "EditProfileActivity";
    private static final int MY_REQUEST_CODE = 3;
    private final String Tag = getClass().getName();
    private final int CAMERA_RESULT = 1;
    Button btn_pic_upload;
    Button btn_profile_upload;
    Context context = MainActivity.this;
    private ImageView imageView;
    private String picturePath;
    private ProgressDialogue progressDialog;
    private ErrorDialog errorDialog;
    ArrayList<StudentInfo> studentInfosArrayList;


    private String name, profession, dateOfBirth, passport, nationalId, email, phone, address, gender = "";
    //  private TextInputLayout inputLayoutName , inputLayoutEmail , inputLayoutPhone , inputLayoutPassport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressDialog = new ProgressDialogue(MainActivity.this);
        errorDialog = new ErrorDialog(context);
        imageView = (ImageView) findViewById(R.id.student_pic);
        btn_pic_upload = (Button) findViewById(R.id.btn_pic_upload);
        btn_profile_upload = (Button) findViewById(R.id.btn_profile_upload);
        //loadImageFromStorage(picturePath);
        DAO dao = new DAO(AppController.getInstance());
        dao.open();
        studentInfosArrayList = dao.gettingAllStudentInfo();

/*
        if(studentInfosArrayList.get()!=null){
            loadImageFromStorage(user.getPicturePath());
        }
*/


        btn_pic_upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        btn_profile_upload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DAO.executeSQL("INSERT OR REPLACE INTO " + TABLE_STUDENT_INFO + "(SImage) " +
                                "VALUES(?)",
                        new String[]{picturePath});
            }
        });


    }

    private void loadImageFromStorage(String path) {

        try {
            File f = new File(path, "profile.jpg");
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            imageView.setImageBitmap(b);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA},
                                    MY_REQUEST_CODE);
                        } else {
                            startCameraIntent();
                        }
                    }
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                        startCameraIntent();
                    }

                } else if (options[item].equals("Choose from Gallery")) {

                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG_FROM_GALLERY);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void startCameraIntent() {
        PackageManager pm = getApplicationContext().getPackageManager();

        if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {

            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);

        } else {

            errorDialog.showDialog("No Camera", "Sorry Your Device Has No Camera Permission");

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startCameraIntent();
            } else {
                errorDialog.showDialog("Error", "Permission Denied");
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        progressDialog.showProgress();
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            try {
                Bitmap photo = (Bitmap) data.getExtras().get("data");

                imageView.setImageBitmap(photo);
                picturePath = saveToInternalStorage(photo);
            } catch (Exception ex) {
                errorDialog.showDialog("Error!", "Try Again");
                Log.d("Main", ex.toString());
            }

        } else if (resultCode == RESULT_OK && requestCode == RESULT_LOAD_IMG_FROM_GALLERY) {
            try {
                final Uri imageUri = data.getData();
                Bitmap selectedImage = decodeUri(MainActivity.this, imageUri, 500);
                imageView.setImageBitmap(selectedImage);
                picturePath = saveToInternalStorage(selectedImage);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                errorDialog.showDialog("Error!", "Try Another Image");
            }

        } else {
            Toast.makeText(MainActivity.this, "You haven't picked Image", Toast.LENGTH_LONG).show();
        }
        progressDialog.hideProgress();
    }

    public static Bitmap decodeUri(Context c, Uri uri, final int requiredSize)
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;

        while (true) {
            if (width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
    }

    private String saveToInternalStorage(Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, "profile.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }

}
