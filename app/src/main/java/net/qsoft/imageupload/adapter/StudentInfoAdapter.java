package net.qsoft.imageupload.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.qsoft.imageupload.R;
import net.qsoft.imageupload.app.AppController;
import net.qsoft.imageupload.model.StudentInfo;

import java.util.List;

public class StudentInfoAdapter extends RecyclerView.Adapter<StudentInfoAdapter.MyViewHolder> {

    private Context mContext;
    private List<StudentInfo> sInfoList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView SName;
        public ImageView SImage;

        public MyViewHolder(View view) {
            super(view);
            SName = (TextView) view.findViewById(R.id.sname);
            SImage = (ImageView) view.findViewById(R.id.show_image);
        }
    }

    public StudentInfoAdapter(Context mContext, List<StudentInfo> itemList) {
        this.mContext = mContext;
        this.sInfoList = itemList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.student_info_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        StudentInfo studentInfo = sInfoList.get(position);
        holder.SName.setText(studentInfo.getSName());

        Glide.with(AppController.getInstance())
                .load(studentInfo.getSImage())
                .into(holder.SImage);
    }

    @Override
    public int getItemCount() {
        return sInfoList.size();
    }
}
